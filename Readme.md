Run `start_api.sh`

The url is http://curve-test.test:1234/

Endpoint: api/v1/exchange

### Endpoint

* **URL** `api/v1/exchange`

* **Method:** `GET`
  
* **URL Params** `from`, `to` - both optional

* **Data Params** None

* **Success Response:** 200 OK 
 
* **Error Response:** 500 Internal Server Error

### TODO/Improve

Caching, Error handling, etc.