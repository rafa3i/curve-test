rm -rf curve-api

git clone https://rafa3i@bitbucket.org/rafa3i/curve-api.git

docker-compose stop
docker-compose up --build -d

cd 'curve-api'

cp .env.example .env

composer install
php artisan key:generate
php artisan optimize:clear